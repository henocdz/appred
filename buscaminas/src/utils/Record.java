/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import java.io.Serializable;

/**
 *
 * @author henocdz
 */
public class Record implements Serializable{
    private String nombre;
    private String nivel;
    private double tiempo;

    public Record(String nivel, String nombre, double tiempo){
        this.nombre = nombre;
        this.tiempo = tiempo;
        this.nivel = nivel;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @return the nivel
     */
    public String getNivel() {
        return nivel;
    }

    /**
     * @return the tiempo
     */
    public double getTiempo() {
        return tiempo;
    }
}
