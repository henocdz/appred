/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author henocdz
 */
public class Tablero {
    /*
    * 1) Posicion random (x,y) para primer celda con numero
    * 2) Numero random (max 8, 8 - numero de celdas no vacias al rededor) que indica el numero de minas al rededor
    * 3) Evaluar si celdas contiguas tienen minas 
    */
    
    private int tablero[][];
    private ArrayList<Coord> closeCells;
    private int filas, columnas;
    Random rand;
    
    public Tablero(int filas, int columnas, int minas){
        
        closeCells = new ArrayList();        
        tablero = new int[filas][columnas];
        this.filas = filas;
        this.columnas = columnas;
        rand = new Random();
        int fila, columna, conValor;     
        
        
        //Inicializar Tablero
        for(int i=0; i < filas; i++){
            for(int j=0; j<columnas; j++){
                // Celda con valor en 10 => Celda sin valor asignado
                tablero[i][j] = 10;
            }
        }
        
        
        System.out.println(filas+":"+columnas);
        
        while(minas>0){
            columna = rand.nextInt(columnas);
            fila = rand.nextInt(filas);
            
            if(!(tablero[fila][columna] == -1) && (tablero[fila][columna] == 10)){
                tablero[fila][columna] = -1;
                minas--;
            }
        }
        
        conValor = 0;
        for(int f=0; f < filas; f++){
            for(int c=0; c<columnas; c++){
                if(tablero[f][c] == -1){
                    continue;
                }
                
                closeCells.clear();

                
                if(c!=0 && f!=0){
                    closeCells.add(new Coord(f-1, c-1));
                }
                
                if(c!=0){
                    closeCells.add(new Coord(f, c-1));
                    if(f<filas-1){
                        closeCells.add(new Coord(f+1, c-1));
                    }
                    
                }
                
                if(f!=0){
                    if(c<columnas-1){
                        closeCells.add(new Coord(f-1, c+1));
                    }
                    closeCells.add(new Coord(f-1, c));
                }
                
                if(f<filas-1){
                    if(c<columnas-1){
                        closeCells.add(new Coord(f+1, c+1));
                    }
                    closeCells.add(new Coord(f+1, c));
                }
                
                if(c<columnas-1){
                    closeCells.add(new Coord(f, c+1));
                }
                
                for(Coord nc:closeCells){
                    if(tablero[nc.i][nc.j] == -1){
                        conValor++;
                    }
                }
                
                tablero[f][c] = conValor;
                conValor = 0;
            }
        }       
    }
    
    public int[][] getTablero(){
       PrettyPrinter p = new PrettyPrinter(System.out);
       
       String stablero[][] = new String[this.filas][this.columnas];
       
       for(int i=0; i < filas; i++){
            for(int j=0; j<columnas; j++){
                stablero[i][j] = String.valueOf(tablero[i][j]);
            }
        }
       
       p.print(stablero);
       return this.tablero;
    }
}
