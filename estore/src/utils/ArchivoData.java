/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import java.io.Serializable;

/**
 *
 * @author henocdz
 */
public class ArchivoData implements Serializable {
    String nombre;
    long size;
    public ArchivoData(String nombre, long tam){
        this.nombre = nombre;
        this.size = tam;
    }
    
    public long getSize(){
        return this.size;
    }
    
    public String getNombre(){
        return this.nombre;
    }
}
