/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author henocdz
 */
public class Compra implements Serializable {

    public boolean salir = false;
    private ArrayList<Producto> productos = null;
    private ArrayList<Producto> catalogo;

    public Compra(boolean salir) {
        this.salir = salir;
    }

    public Compra(ArrayList<Producto>  catalogo) {
        this.catalogo = catalogo;
        this.productos = new ArrayList();
    }


    public int getTotalItems(){
        int total = 0, i;
        for(Producto p : catalogo){
            total += p.getSeleccionados();
        }
        return total;
    }
    
    public boolean addProducto(Producto producto) {
        if (producto.getDisponibles() > 0) {
            producto.unoMenos(); //Uno menos disponible
            producto.unoMas(); //Uno mas seleccionado
            return true;
        }
        /*Producto productoLista = null;
         for (Producto p : catalogo.getProductos()) {
         //Checar si el producto existe en el catalogo
         if (p.getId() == producto.getId()) {
         productoLista = p;
         }
         }

         //Checar si el producto existe en el carrito
         for (Producto p : productos) {
         if (p.getId() == producto.getId()) {
         p.unoMas();
         return true;
         }
         }
        
         if(productoLista == null){
         productos.add(producto);
         }*/

        return false;
    }

    public ArrayList<Producto> getProductos() {
        return this.catalogo;
    }
   
    public void printProductos(){
        for(Producto p : catalogo){
            System.out.println(p.getNombre() + " >> " +  p.getSeleccionados() + "|||" + p.getDisponibles());
        }
    }
}
