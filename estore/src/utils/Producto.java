/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.Serializable;

/**
 *
 * @author henocdz
 */
public class Producto implements Serializable {

    private String cover;
    private String nombre;
    private double precio;
    private int disponibles;
    private int seleccionados;
    private int id;

    public Producto(int id, String nombre, String cover, double precio, int disponibles) {
        this.id = id;
        this.nombre = nombre;
        this.cover = cover;
        this.precio = precio;
        this.seleccionados = 0;
        this.disponibles = disponibles;
    }

    public int getId() {
        return this.id;
    }

    /**
     * @return the cover
     */
    public String getCover() {
        return cover;
    }

    public String getCoverUrl(){
        String dir = System.getProperty("user.dir");
        String path = "file://" + dir + "/cliente/" + cover;
        //System.out.println(path);
        return path;
    }
    
    /**
     * @param cover the cover to set
     */
    public void setCover(String cover) {
        this.cover = cover;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the precio
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * @return the disponibles
     */
    public int getDisponibles() {
        return disponibles;
    }

    /**
     * @param disponibles the disponibles to set
     */
    public void setDisponibles(int disponibles) {
        this.disponibles = disponibles;
    }
    
    public int getSeleccionados(){
        return this.seleccionados;
    }

    public void unoMasDisponible(){
        this.disponibles++;
    }
    
    public void unoMenosSeleccionado(){
        this.seleccionados--;
    }
    
    public void unoMenos(){
        //Uno menos disponible
        this.disponibles--;
    }
    
    public void unoMas() {
        //Uno mas seleccionado
        this.seleccionados++;
    }
    
}
