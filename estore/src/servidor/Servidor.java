/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.ArchivoData;
import utils.ArchivoFTP;
import utils.Compra;
import utils.Producto;

/**
 *
 * @author henocdz
 */
public class Servidor {

    public static String formatSize(long bytes) {
        double kilobytes = (bytes / 1024);
        double megabytes = (kilobytes / 1024);
        double gigabytes = (megabytes / 1024);

        if (gigabytes > 1) {
            return gigabytes + " GB";
        } else if (megabytes > 1) {
            return megabytes + " MB";
        } else {
            return kilobytes + " KB";
        }
    }

    public static ArrayList getCatalogo() {
        ArrayList<Producto> catalogo = new ArrayList();
        File catxt = new File("catalogo.txt");
        try {
            BufferedReader br = new BufferedReader(new FileReader(catxt));
            String producto;
            while ((producto = br.readLine()) != null) {
                String[] iProducto = producto.split("\\|");
                catalogo.add(new Producto(Integer.valueOf(iProducto[0]),
                        iProducto[1],
                        iProducto[2],
                        Double.parseDouble(iProducto[3]),
                        Integer.parseInt(iProducto[4])
                ));
            }
            br.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }

        return catalogo;
    }

    public static void main(String[] args) throws Exception {
        ServerSocket ss = new ServerSocket(4000);
        System.out.println("Servicio iniciado, esperando por cliente...");
        ArrayList<Producto> catalogo;
        catalogo = getCatalogo();

        for (;;) {
            Socket cl = ss.accept();
            boolean flag = true;
            cl.setSoTimeout(0);

            File dir = new File("./servidor/");
            File[] archs = dir.listFiles();
            ArrayList<ArchivoFTP> archivos = new ArrayList();

            for (File archivo : archs) {
                archivos.add(new ArchivoFTP(archivo));
            }

            ObjectOutputStream oos = new ObjectOutputStream(cl.getOutputStream());
            ObjectInputStream ios = new ObjectInputStream(cl.getInputStream());
            ArrayList<ArchivoData> adata = new ArrayList();

            for (ArchivoFTP archivo : archivos) {
                adata.add(new ArchivoData(archivo.getNombre(), archivo.getSize()));
            }

            oos.writeObject(adata);
            oos.flush();

            BufferedOutputStream bos;
            for (ArchivoFTP archivo : archivos) {
                bos = new BufferedOutputStream(cl.getOutputStream());
                BufferedInputStream bis = new BufferedInputStream(
                        new FileInputStream(archivo.getFile())
                );

                byte[] buf = new byte[1024];
                long leidos = 0;

                long bloque = (bis.available() >= 1024) ? 1024 : bis.available();
                long archivoLength = bis.available();

                int b_leidos;

                while ((b_leidos = bis.read(buf, 0, buf.length)) != -1) {
                    bos.write(buf, 0, b_leidos);
                    bos.flush();
                    // TODO: Actualizar porcentaje enviado
                    leidos += bloque;
                    //System.out.println(archivo.getNombre() + " | " + formatSize(leidos) + " / " + archivo.getSizeString());
                    bloque = (bis.available() >= 1024) ? 1024 : bis.available();
                }//while
                bis.close();

                archivo.setEstado("Enviado");
            }

            oos.writeObject(catalogo);
            oos.flush();

            while (flag) {
                System.out.println("--------------------------------------");
                //Espera objecto con informacion de compra o 
                //salida de Cliente
                Compra c;
                try {
                    c = (Compra) ios.readObject();
                } catch (Exception e) {
                    flag = false;
                    continue;
                }

                flag = !c.salir;
                if (flag) {
                    //c.printProductos();
                    System.out.println(">>> NUEVA COMPRA <<<");
                    for (Producto producto : c.getProductos()) {
                        if (producto.getSeleccionados() > 0) {
                            //System.out.println(producto.getNombre());
                            for (Producto p : catalogo) {
                                if (p.getId() == producto.getId()) {
                                    System.out.println("\t" + p.getNombre() + " | " + producto.getSeleccionados());
                                    p.setDisponibles(p.getDisponibles() - producto.getSeleccionados());
                                }
                            }
                        }

                        //Buscar productos y validar si hay existencia de producto
                        //Actualizar inventario en el catalogo
                        //Regresar catalogo actualizado con disponibilidad de inventario
                    }

                    oos.reset();
                    oos.writeObject(catalogo);
                    oos.flush();
                    oos.reset();
                }
            }

            System.out.println("No mas compras :)");

        }//infinity
    }//main

}
