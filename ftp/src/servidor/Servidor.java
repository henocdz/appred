package servidor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import utils.ArchivoData;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author henocdz
 */

public class Servidor {
    public static void main(String[] args) throws Exception{
        ServerSocket ss = new ServerSocket(4000);
        System.out.println("Servicio iniciado, esperando por cliente...");

        File here = new File("");
        String dir = here.getAbsolutePath() + "/recibidos/";
        for(;;){
                Socket cl = ss.accept();
                cl.setSoTimeout(30000);
                int totalArchivos = 0;

                ObjectInputStream ois = new ObjectInputStream(cl.getInputStream());
                ArrayList<ArchivoData> iArchivos = (ArrayList) ois.readObject();
                totalArchivos = iArchivos.size();
                

                //Input stream del socket
                BufferedInputStream bis = new BufferedInputStream(cl.getInputStream());
                for(int f=0; f<totalArchivos; f++){
                    ArchivoData archinfo = iArchivos.get(f);
                    String archivoPath = dir + archinfo.getNombre();
                    File archivo = new File(archivoPath);
                    long archivo_size = archinfo.getSize();
                    //OutPutStream para escribir archivo en local
                    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(archivoPath));

                    byte[] buf = new byte[1024];
                    int bleidos;
                    long leidos=0;
                    
                    //bos.write(buf, 0, buf.length);
                    while((bleidos=bis.read(buf, 0, buf.length))!=-1){
                        bos.write(buf, 0, bleidos);
                        bos.flush();
                        leidos += bleidos;
                        if(leidos == archivo_size){
                            bos.close();
                            break;
                        }
                    }//while
                    
                    System.out.println("Se guardo el archivo: " + archinfo.getNombre() + " / Tamaño: " + archivo.length());
                }
                        
                bis.close();
        }//infinity
    }//main
    
}
