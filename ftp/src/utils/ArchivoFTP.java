/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import java.io.File;

/**
 *
 * @author henocdz
 */
public class ArchivoFTP {
    File archivo;
    String estado;
    public ArchivoFTP(File archivo){
        this.archivo = archivo;
        this.estado = "En cola";
    }
    
    public File getFile(){
        return this.archivo;
    }
    
    public String getNombre(){
        return this.archivo.getName();
    }
    
    public long getSize(){
        return this.archivo.length();
    }
    
    public String getPath(){
        return this.archivo.getAbsolutePath();
    }
    
    public String getSizeString(){
        long bytes = this.archivo.length();

        double kilobytes = (bytes / 1024);
        double megabytes = (kilobytes / 1024);
        double gigabytes = (megabytes / 1024);
        
        if(gigabytes > 1){
            return gigabytes + " GB";
        }else if(megabytes > 1){
            return megabytes + " MB";
        }else{
            return kilobytes + " KB";
        }
    }
    
    public String getEstado(){
        return this.estado;
    }
    
    public void setEstado(String edo){
        this.estado = edo;
    }
}
