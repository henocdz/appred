import threading
import socket
import sys
import utils

class Server(object):
    def __init__(self, ip, port):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.socket.bind( (ip, port) )
            self.socket.listen(2)
        except:
            print("Failed to connect: ", ip, ':', port)
            exit(1)


    def go(self):
        while True:
            client, addr = self.socket.accept()
            print('Connected with ' + addr[0] + ':' + str(addr[1]))

            data = utils.recvall(client)
            num_set = utils.bytearray_to_list(data)
            num_set.sort()
            client.sendall( utils.list_to_bytearray(num_set) )

            client.shutdown(socket.SHUT_WR | socket.SHUT_RD)
            client.close()
        self.socket.close()


def shitty(ip, port):
    server = Server(ip, port)
    server.go()

def main():
    IP, PORT = utils.get_ip_port()
    for x in range(0, 5):
        print("Server on: http://localhost:" + str(PORT + x))
        thread = threading.Thread(target=shitty, args=(IP, PORT + x))
        thread.start()
    thread.join()

#Pos...
main()