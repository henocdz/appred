import socket
import json
import sys

def recvall(_socket, bytes=4096):
    """Receive all socket data"""
    data = b""
    while True:
        part = _socket.recv(bytes)
        # print(part)
        if not part:    break
        data += part

    # _socket.shutdown(socket.SHUT_WR)
    return data

def list_to_bytearray(data):
    return bytearray(json.dumps(data), 'utf-8')

def get_ip_port():
    ip = sys.argv[1]
    port = sys.argv[2]

    try:
        port = int(port)
    except:
        pass

    return ip, port

def bytearray_to_list(barray):
    data = barray.decode('utf-8')
    _list = json.loads(data)
    return _list