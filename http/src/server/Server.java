package server;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.stream.Stream;

public class Server {

    public static final int PUERTO = 8000;
    public static final Map<Integer, String> HTTP_RESPONSE;

    static {
        HTTP_RESPONSE = new HashMap<Integer, String>();
        HTTP_RESPONSE.put(500, "HTTP/1.0 500 Internal Server Error");
        HTTP_RESPONSE.put(501, "HTTP/1.0 501 Not Implemented");
        HTTP_RESPONSE.put(200, "HTTP/1.0 200 OK");
        HTTP_RESPONSE.put(401, "HTTP/1.0 401 UnAuthorized");
        HTTP_RESPONSE.put(404, "HTTP/1.0 404 Not Found");
        HTTP_RESPONSE.put(405, "HTTP/1.0 405 Method not allowed");
    }

    ServerSocket ss;

    class Manejador extends Thread {

        protected Socket socket;
        protected PrintWriter pw;
        protected BufferedOutputStream bos;
        protected BufferedReader br;
        protected String FileName;

        public Manejador(Socket _socket) throws Exception {
            this.socket = _socket;
        }

        private void formatLine(Map req, String line, Iterator<String> lines) {
            String parts[] = line.split(": ");

            if (parts.length == 2) {
                if (parts[0].toLowerCase().equals("content-disposition")) {
                    addParam(req, line, lines);
                    return;
                }

                req.put(parts[0], parts[1]);
            } else if (!line.isEmpty()) {
                //System.out.println("Uknown header format :: " + line);
            }

            return;
        }

        private void formatMethod(Map req, String line) {
            String parts[] = line.split(" ");
            req.put("method", parts[0].toUpperCase());
            req.put("uri", parts[1].substring(1, parts[1].length()));
            req.put("protocol", parts[2]);

            queryParams((String) req.get("uri"), req);
        }

        private void queryParams(String uri, Map req) {
            int question = uri.indexOf("?");

            if (question >= 0) {
                uri = uri.substring(question + 1);
                String params[] = uri.split("&");
                for (int i = 0; i < params.length; i++) {
                    String param[] = params[i].split("=");

                    try {
                        req.put("param_" + param[0], param[1]);
                    } catch (Exception e) {
                        continue;
                    }
                }
            }
        }

        private void printRequest(Map req) {
            Set ks = req.keySet();
            Iterator keys = ks.iterator();
            String key;
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>");
            while (keys.hasNext()) {
                key = (String) keys.next();
                //if (key.indexOf("param_") >= 0) {
                System.out.println(key + " = " + (String) req.get(key));
                //}
            }
            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<");
        }

        private void addParam(Map req, String name, Iterator<String> lines) {
            String parts[] = name.split("; name=");
            String val = lines.next();
            val = lines.next();
            req.put("param_" + parts[1], val.replace("\"", ""));
        }

        public void run() {
            try {
                br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                bos = new BufferedOutputStream(socket.getOutputStream());
                pw = new PrintWriter(new OutputStreamWriter(bos));

                //String line = br.readLine();
                String line = null;
                /**
                 * ***********
                 */
                //Format all incoming "lines" and insert into hashmap
                Stream<String> lines = br.lines();
                Iterator<String> iLines = lines.iterator();
                int emptyLines = 0;

                Map<String, String> req = new HashMap<String, String>();
                formatMethod(req, iLines.next());

                //System.out.println(req.get("uri"));
                while (iLines.hasNext()) {
                    line = iLines.next();
                    //System.out.println(line);
                    formatLine(req, line, iLines);
                    if (line.isEmpty()) {
                        break;
                    }
                    /*if (line.isEmpty() && !data) {
                     emptyLines++;
                     System.out.println(line);
                     iLines.next();
                     if (emptyLines > 1) {
                     break;
                     }
                     }else{
                     emptyLines++;
                     }*/
                }

                printRequest(req);
                /**
                 * *************
                 */
                if (req.get("method").equals("GET")) {
                    String filePath = getFilePath(req.get("uri"));
                    sendFile(filePath, socket, pw);
                    pw.flush();
                } else if (req.get("method").equals("POST")) {
                    pw.print(HTTP_RESPONSE.get(401));
                    pw.print("\n" + req.get("body"));
                    pw.flush();
                } else if (req.get("method").equals("HEAD")) {
                    pw.print(HTTP_RESPONSE.get(500));
                    pw.flush();
                } else {
                    pw.print(HTTP_RESPONSE.get(501));
                    pw.flush();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public String getFilePath(String path) {
            int indexOfQuestion = path.indexOf("?");
            if (indexOfQuestion >= 0) {
                path = path.substring(0, indexOfQuestion);
            }

            if (path.isEmpty()) {
                path = "index.html";
            }
            
            return "templates/" + path;
        }

        public void sendFile(String fileName, Socket sc, PrintWriter pw) {
            //System.out.println(fileName);
            int fSize = 0;
            byte[] buffer = new byte[4096];
            try {
                DataOutputStream out = new DataOutputStream(sc.getOutputStream());

                //sendHeader();
                FileInputStream f = new FileInputStream(fileName);
                //pw.write(HTTP_RESPONSE.get(200));
                //pw.flush();
                int x = 0;
                while ((x = f.read(buffer)) > 0) {
                    //		System.out.println(x);
                    out.write(buffer, 0, x);
                }
                out.flush();
                f.close();
            } catch (FileNotFoundException e) {
                sendFile("templates/errors/404.html", sc, null);
                //msg.printErr("Transaction::sendResponse():1", "El archivo no existe: " + fileName);
                //pw.write(HTTP_RESPONSE.get(404));
            } catch (IOException e) {
                sendFile("templates/errors/500.html", sc, null);
                //pw.write(HTTP_RESPONSE.get(500));
            }

        }

    }

    public Server() throws Exception {
        this.ss = new ServerSocket(PUERTO);
        System.out.println("Servidor iniciado...");
        System.out.println("Esperando por Cliente....");
        for (;;) {
            Socket accept = ss.accept();
            new Manejador(accept).start();
        }
    }

    public static void main(String[] args) throws Exception {
        Server sWEB = new Server();
    }

}
